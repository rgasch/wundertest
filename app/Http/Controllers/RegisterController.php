<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Cookie;
use View;


class RegisterController extends Controller
{
    public function index()
    {
        $step = (int)Cookie::get('step');
        if (!$step) {
            $step = 1;
        }

        $view = 'register_step_' . $step;

        // Reset cookies at end of process so that the entire process can start over
        if ($step == 4) {
            \Cookie::queue(\Cookie::forget('step'));
            \Cookie::queue(\Cookie::forget('uid'));
        }

        return view($view);
    }
}
