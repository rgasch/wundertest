<?php

namespace App\Http\Controllers;

use App\Models\User;
use GuzzleHttp\Client;
use Cookie;


class RegisterFormController extends Controller
{
    private $oneYearInMinutes = 60*24*365;

    public function store()
    {
        $input = request()->all();
        $step  = (int)Cookie::get('step');

        if (!$step) {
            $step = 1;
        }

        $processFunc = "processStep" . $step;
        $rc = $this->$processFunc($input);

        if ($rc) {
            Cookie::queue('step', $step + 1, $this->oneYearInMinutes);
        }

        return redirect()->back();
    }


    // Step1: new registration has started
    private function processStep1(array $input) : bool
    {
        $user = new User();
        $user->fill($input);
        $user->save();

        // Save current user ID in cookie
        Cookie::queue('uid', $user->id, $this->oneYearInMinutes);

        return true;
    }


    // Step2: address
    private function processStep2(array $input) : bool
    {
        $uid  = (int)Cookie::get('uid');
        if (!$uid) {
            throw new \InvalidArgumentException("Unable to retrieve cookie for [uid]");
        }
        $user = User::find($uid);
        if (!$user) {
            throw new \InvalidArgumentException("Unable to retrieve user for ID [$uid]");
        }

        $user->fill($input);
        $user->save();

        return true;
    }


    // Step3: payment info
    private function processStep3(array $input) : bool
    {
        $uid  = (int)Cookie::get('uid');
        if (!$uid) {
            throw new \InvalidArgumentException("Unable to retrieve cookie for [uid]");
        }
        $user = User::find($uid);
        if (!$user) {
            throw new \InvalidArgumentException("Unable to retrieve user for ID [$uid]");
        }

        $user->fill($input);
        $user->save();

        $rc = $this->getRemotePaymentId($uid, $input);
        if (!$rc) {
            return false;
        }

        $user->payment_data_id = $rc;
        $user->save();

        return true;
    }


    private function getRemotePaymentId(int $uid, array $input) : string
    {
        $url      = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com';
        $postData = [
            'customerId' => $uid,
            'iban'       => $input['payment_iban'],
            'owner'      => $input['payment_account_owner']
        ];

        try {
            $client   = new Client(['base_uri' => $url]);
            $uri      = '/default/wunderfleet-recruiting-backend-dev-save-payment-data';
            $response = $client->request('POST', $uri, ['json' => $postData]);
        } catch (\Exception $e) {
            return '';
        }

        $body = json_decode($response->getBody(), true);

        return $body['paymentDataId'] ?? '';
    }
}
