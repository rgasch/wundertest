@extends('layout')

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center h-100">
            <div class="card">
                <div class="card-header text-center">
                    Step 1
                </div>
                <div class="card-body w-100">
                    <div class="col-12">
                        <form action="{{ route('register:user.get') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" name="name_first" id="nameFirst" class="form-control" placeholder="First Name" maxlength="255" required autofocus />
                            </div>
                            <div class="form-group">
                                <input type="text" name="name_last" id="nameLast" class="form-control" placeholder="Last Name" maxlength="255" required />
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone" maxlength="255" required />
                            </div>
                            <div class="form-group">
                                <button class="btn btn-lg btn-primary btn-block" type="submit">Continue</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
