@extends('layout')

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center h-100">
            <div class="card">
                <div class="card-header text-center">
                    Step 2
                </div>
                <div class="card-body w-100">
                    <div class="col-12">
                        <form action="{{ route('register:user.get') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" name="address_street" id="addressStreet" class="form-control" placeholder="Street" maxlength="255" required autofocus />
                            </div>
                            <div class="form-group">
                                <input type="text" name="address_street_number" id="addressStreetNumber" class="form-control" placeholder="Street Number" maxlength="255" required />
                            </div>
                            <div class="form-group">
                                <input type="text" name="address_zip" id="addressZip" class="form-control" placeholder="Zip" maxlength="255" required />
                            </div>
                            <div class="form-group">
                                <input type="text" name="address_city" id="addressCity" class="form-control" placeholder="City" maxlength="255" required />
                            </div>
                            <div class="form-group">
                                <button class="btn btn-lg btn-primary btn-block" type="submit">Continue</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
