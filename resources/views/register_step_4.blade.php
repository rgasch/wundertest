@extends('layout')

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center h-100">
            <div class="card">
                <div class="card-header text-center">
                    Payment Complete!
                </div>
                <div class="card-body w-100">
                    <div class="col-12">
                        <div class="alert alert-success">
                            Thank you for registring with us. Your payment has been sent!
                        </div>
                        <div class="alert alert-info mt-3">
                            Your information has been cleared from this browser, you perform
                            this process again by reloading the page.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
