## Wunder Mobility Test Project

### Requirements
- Mysql/MariaDB current version.
- PHP 7.4 or 8.0
- Apache with mod-php and mod-rewrite enabled.
- I ran this through a local vhost (document root for me is */var/www/wunder.lan/public*), however it should also run from a subdirectory.
- Everything has been committed to the git archive, but in case of issues, you might want to run *composer update* to ensure that you have all required 
dependencies installed.

### Installation
- create a new database in mysql for this project (thereafter called *wunderdb*)
- download project using git
- *cd wunder*
- *chown -R www-data:www-data \** (this assumes that *www-data* is the process owner of your web server)
- edit *.env* and set the appropriate DB settings (DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, DB_PASSWORD) for your setup
- create the tables by doing *php artisan migrate* OR *mysql -D wunderdb -u root -p < sql/wunder.sql*
- go to *http://wunder.lan* (or whatever domain/setup/port/etc. you chose for this installation)
- Please note: you should have mod-rewrite enabled on your web server

### Overview
The project assignment has been implemented using a new Laravel 8 installation. The following files have been added and/or customized:

- app\Http\Contollers\RegisterController: The Controller responsible for rendering the correct view for each step.
- app\Http\Contollers\RegisterFormController: The Controller holding the code to process the form requests.
- database\migrations\2014_10_12_000000_create_users_table: removed some default columns, added columns required for this test exercise.
- resources\views\layout.blade.php: the main HTML5 layout used.
- resources\Views\register_step_*.blade: the template code for each step.
- routes/web.php: added new routes to call the new controller code.


### How does it work

- The system is implemented using the MVP pattern, Eloquent (Laravel's ORM layer) and Blade (Laravel's template engine) layer. 
- The state/step is saved to a local cookie, which allows the app to know which form is to be displayed next.
- When submitting the first page, a new user is created and the user ID is set to a cookie. 
- This allows the app to know which user's data we're working on and update the user after each step.
- After the last page, a success message is displayed and cookies are cleared. The process can then be started again.
- The cookies duration/lifetime is 1 year, an arbitrarily chosen value.


### What improvements could be made

- In terms of performance there's not much that can be done, this is too simple an app to really get significant performance 
improvements through optimizations, although it would be possible to cache the user object, which would save a SQL statement 
upon each page load for steps 2 and 3.
- The validation at this point is only front-end HTML5 validation, the backend will accept any value you pass to it. This 
could result in SQL errors if the HTML5 validation were bypassed and strings, which are too long for the DB columns, were 
passed to the backend.
- The DB code currently does not check for errors; if it fails, an (uncaught) exception is thrown. 
- The Guzzle request for the POST request to obtain the paymentId is extremely rudimentary; this could certainly be improved; 
right now any exceptions are caught and a boolean value of false is passed back to the calling function. 
- All font-end texts are currently hardcoded. 
- Following from the hardcoded texts, there is no support for internationalization. 
- The HTML layout could be improved (proper centering, responsive display for mobiles, etc.)
- Currently the state is saved in cookies without a way to reset these during the process. This means, that if you wish to 
start over before the last step, you have to manually delete these cookies (however, they are cleared after the last step).
- If you clear cookies, manually, there is no way to continue the editing of a partially saved/completed user. 

